# Minecraft 3D Build Viewer

## Why have I made this?
To keep it simple: I just wanted to learn some JS.

## Can I learn from you/this project?
Please don't. I have done this to learn myself so you propably would just copy a lot of errors.

## Can I deploy this myself?
Of course! It may run like crap \(I don't know yet\) but go for it!

## How do I add blocks/items/textures/builds?
### Blocks/Items
To add a block or item, edit the objects file under "./objects.json" following this principle:
```json
{
  "textures": [
    "texture_top",
    "texture_bottom",
    "texture_nort",
    "texture_east",
    "texture_south",
    "texture_west"
  ],
  "name": "The name that will be displayed for this block/item",
  "description": "Put your block/item description here!",
  "materials": [
    [
      "item_one", 1
    ],
    [
      "item_two", 3
    ],
    [
      "item_three", 3
    ],
    [
      "item_four", 7
    ]
  ]
}
```
You can just copy this and edit it to your liking!\
Note:
  - The texture(s) go in the order of \[Top, Bottom, North, East, South, West\]
  - With only one texture, like cobblestone or dirt or items, just do \["YourTexture"\]
  - The materials list is just to show what you need to craft that block so it can be added up in the end to show the total needed resources
  - The materials are always: ["MaterialName", Quantity] (Don't put other craftables or the item itself in here, )
  - MaterialName's can be added in "./items.json"

### Textures
Well, to add textures, put them in either\
"./textures/blocks" or "./textures/items" or "./textures/ui"\
Although I can't recommend using the ui folder as this is (like the name suggests)\
only used internally for UI elements, like the Ruby in the sliders!

### Builds
To add an entire build, create a new .json file under "./builds"\
In this file, add the following type of structure:
```json
[
  {
    "block": "cobblestone",
    "position": {
      "x": 0,
      "y": 0,
      "z": 0
    }
  },
  {
    "block": "piston",
    "position": {
      "x": 0,
      "y": 3,
      "z": 0
    },
    "facing": "north"
  }
]
```
In this example I set a cobblestone block at 0, 0, 0 and a piston at 0, 3, 0\
the piston is facing north, you only need this "facing" attribute for blocks that\
have multiple textures though. A dirt or cobblestone block for example won't show\
any difference at all.\
Note:
  - the "block" paramenter takes the values from "./objects.json" so make sure its defined there too

#### I want to add, I am in NO way affiliated with Mojang or Microsoft!
#### All rights to their respective owners!
