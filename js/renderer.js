// Getting the clock so everything moves nicely independent of the FPS
var clock = new THREE.Clock();

// Creating the scene and camera
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 10000 );

// Some variables to keep track of the current build!
var existingBlocks = [];
var highestBlock = 1;
var lowestBlock = 0;

// The Variable that keeps track of all the blocks/items
var objects = null;

// Creating the WebGL Renderer
renderer = new THREE.WebGLRenderer({ alpha: true });
renderer.setClearColor( 0x40529499, 1 );
var viewport = document.getElementById('viewport');
renderer.setSize( viewport.clientWidth, viewport.clientHeight );
viewport.appendChild( renderer.domElement );

// Adding the actual blocks!
// How exciting!
function addBlock(blockTextures, coordX, coordY, coordZ, facing) {
    var rotX, rotY, rotZ;

    switch (facing) {
      case "up":
        rotX = 0;
        rotY = 0;
        rotZ = 0;
        break;
      case "down":
        rotX = Math.PI;
        rotY = 0;
        rotZ = 0;
        break;
      case "north":
        rotX = Math.PI / -2;
        rotY = 0;
        rotZ = 0;
        break;
      case "east":
        rotX = Math.PI / -2;
        rotY = 0;
        rotZ = Math.PI / -2;
        break;
      case "south":
        rotX = Math.PI / -2;
        rotY = 0;
        rotZ = Math.PI;
        break;
      case "west":
        rotX = Math.PI / -2;
        rotY = 0;
        rotZ = Math.PI / 2;
        break;
      default:
        rotX = 0;
        rotY = 0;
        rotZ = 0;
        break;
    }

    // Create the geometry for the block
    var geometry = new THREE.BoxBufferGeometry( 1, 1, 1 );
    // Load the texture(s) and set its min/mag Filter

    if (blockTextures.length == 1) {
      // Set the texture loader
      let loader = new THREE.TextureLoader();

      // Create an empty array
      materialArray = []

      // Fill the entire array with the one texture
      for (i = 0; i < 6; i++) {
        materialArray.push(
          new THREE.MeshBasicMaterial( { map: loader.load('textures/' + blockTextures[0] + '.png')} )
        );
      }

      // Make the texture render correctly
      for (i = 0; i < materialArray.length; i++) {
        materialArray[i].map.minFilter = THREE.NearestFilter;
        materialArray[i].map.magFilter = THREE.NearestFilter;
        materialArray[i].transparent = true;
      }

      // Create the cube
      cube = new THREE.Mesh( geometry, materialArray );
    }
    else {
      // Set the texture loader
      let loader = new THREE.TextureLoader();

      // Create the array with all 6 sides
      let materialArray = [
        new THREE.MeshBasicMaterial( { map: loader.load('textures/' + blockTextures[2] + '.png')} ),  // Front
        new THREE.MeshBasicMaterial( { map: loader.load('textures/' + blockTextures[3] + '.png')} ),  // Back
        new THREE.MeshBasicMaterial( { map: loader.load('textures/' + blockTextures[0] + '.png')} ),  // Top
        new THREE.MeshBasicMaterial( { map: loader.load('textures/' + blockTextures[1] + '.png')} ),  // Bottom
        new THREE.MeshBasicMaterial( { map: loader.load('textures/' + blockTextures[4] + '.png')} ),  // Left
        new THREE.MeshBasicMaterial( { map: loader.load('textures/' + blockTextures[5] + '.png')} )  // Right
      ];

      // Make the textures render correctly
      for (i = 0; i < materialArray.length; i++) {
        materialArray[i].map.minFilter = THREE.NearestFilter;
        materialArray[i].map.magFilter = THREE.NearestFilter;
        materialArray[i].transparent = true;
      }

      // Create the cube
      cube = new THREE.Mesh( geometry, materialArray );
    }

    // Create the block and set its position and rotation
    cube.position.x = coordX;
    cube.position.y = coordY;
    cube.position.z = coordZ;

    // Rotating the block (NOTE: Radians are weird AF)
    cube.rotation.setFromVector3(new THREE.Vector3(rotX, rotY, rotZ));

    // Add the block to the scene and to the list of blocks!
    scene.add( cube );
    existingBlocks.push(cube);
    // Keep track of the highest and lowest blocks
    if (coordY > highestBlock) {
      highestBlock = coordY;
    }
    if (coordY < lowestBlock) {
      lowestBlock = coordY;
    }
}

// TODO: FIX The heigthCutOffSlider!
function startBuilding(buildFile) {
  for (blockToPlace of buildFile) {
    var blockInfo = objects[blockToPlace['block']];
    if ("facing" in blockToPlace) {
      console.info('DEBUG: Placing ' + blockInfo['name'] + " at " + blockToPlace['position']['x'] + ", " + blockToPlace['position']['y'] + ", " + blockToPlace['position']['z'] + " facing: " + blockToPlace['facing']);
      addBlock(blockInfo['textures'], blockToPlace['position']['x'], blockToPlace['position']['y'], blockToPlace['position']['z'], blockToPlace['facing']);
    } else {
      console.info('DEBUG: Placing ' + blockInfo['name'] + " at " + blockToPlace['position']['x'] + ", " + blockToPlace['position']['y'] + ", " + blockToPlace['position']['z']);
      addBlock(blockInfo['textures'], blockToPlace['position']['x'], blockToPlace['position']['y'], blockToPlace['position']['z']);
    }
  }

  // Make the slider the right size!
  var heigthCutOffSlider = document.getElementById('heigthCutOffSlider');
  heigthCutOffSlider.max = highestBlock;
  heigthCutOffSlider.min = lowestBlock-1;
  heigthCutOffSlider.value = highestBlock;
}

function getBuildFile(availableObjects) {
  objects = availableObjects;
  loadJson('../builds/t_flip_flop.json', startBuilding)
}

function loadJson(path, callback) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var obj = JSON.parse(this.responseText);
      console.info('DEBUG: Loaded json from ' + path);
      callback(obj);
      return;
    }
  };
  xmlhttp.open("GET", path, true);
  xmlhttp.send();
}

// Setting up the camera
controls = new THREE.OrbitControls(camera, renderer.domElement)
controls.target.set( 0, highestBlock/2, 0 );
controls.enablePan = false;
controls.rotateSpeed = 0.5;

camera.position.set( 0, 1, 10 );
controls.update();

// Just to make sure that the user resizing the window wont fuck everything up :D
window.addEventListener( 'resize', onWindowResize, false );
function onWindowResize() {
  camera.aspect = viewport.clientWidth / viewport.clientHeight;
  camera.updateProjectionMatrix();
  renderer.setSize( viewport.clientWidth, viewport.clientHeight );
}

// First lets wait for the document to fully load!
window.onload = function () {

  // Populating our objects and starting the build-process
  loadJson("../objects.json", getBuildFile);

  // TODO: Togglable axis-helper!
  /*
  var axesHelper = new THREE.AxesHelper( 5 );
  scene.add( axesHelper );
  scene.remove(axesHelper);
  */

  // Turning just SOME blocks invisible!
  heigthCutOffSlider.oninput = function() {
    for (i of existingBlocks) {
      if (i.position.y > heigthCutOffSlider.value) {
        for (i = 0; i < 6; i++) {
          i.material[i].opacity = 0.3;
        }
      } else {
        i.material.opacity = 1;
      }
    }
  };
};

// Animating everything (This needs to happen to see anything)
function animate() {
   requestAnimationFrame( animate );

   renderer.render( scene, camera );
}
animate();
